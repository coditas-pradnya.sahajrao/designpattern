function Member(name) {
    this.name = name;
    this.chatroom = null;
}

Member.prototype = {
    send: function(message, toMember) {
        this.chatroom.send(message, this, toMember)
    },
    recieve: function(message, fromMember) {
        console.log(`${fromMember.name} to ${this.name}:${message}`);
    }
}

function chatroom() {
    this.members = {};
}

chatroom.prototype = {
    addMember: function(member) {
        this.members[member.name] = member
        member.chatroom = this
    },
    send: function(message, fromMember, toMember) {
        toMember.recieve(message, fromMember)
    }
}

const chat = new chatroom()

const bob = new Member("Bob")
const john = new Member("John")
const tim = new Member("Tim")

chat.addMember(bob)
chat.addMember(tim)
chat.addMember(john)

bob.send("Hey John", john);