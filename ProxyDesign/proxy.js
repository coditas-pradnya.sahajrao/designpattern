function CryptocurrencyAPI() {
    this.getValue = function(coin) {
        console.log("Calling External Api...")
        switch (coin) {
            case "Bitcoin":
                return "$8,500";
            case "Litecoin":
                return "$50";
            case "Ethereum":
                return "$175";

        }
    }
}

// const api = new CryptocurrencyAPI();
// console.log(api.getValue("Bitcoin"));
// console.log(api.getValue("Ethereum"));
// console.log(api.getValue("Litecoin"));

function CryptocurrencyProxy() {
    this.api = new CryptocurrencyAPI()
    this.cache = {}
    this.getValue = function(coin) {
        if (this.cache[coin] == null) {
            this.cache[coin] = this.api.getValue(coin)
        }
        return this.cache[coin]
    }
}

const proxy = new CryptocurrencyProxy()
console.log(proxy.getValue("Bitcoin"))