function Developer(name) {
    this.name = name;
    this.type = "Developer";
}

function Tester(name) {
    this.name = name;
    this.type = "Tester";
}

function EmployeeFactory() {
    this.create = (name, type) => {
        switch (type) {
            case 1:
                return new Developer(name)
                break;
            case 2:
                return new Tester(name)
                break;
        }
    }
}

function say() {
    console.log(`hi i am ${this.name} and i m a ${this.type} `)
}
const employeeFactory = new EmployeeFactory()
const employees = [];

employees.push(employeeFactory.create("tejas", 1))

employees.forEach(emp => {
    say.call(emp)
})