function process(state) {
    this.state = state
}
const singleton = (function() {
    function processManger() {
        this.numProcess = 0;
    }
    let pManger

    function createProcessManger() {
        pManger = new processManger()
        return pManger;
    }
    return {
        getProcessManager: () => {
            if (!pManger)
                pManger = createProcessManger()
            return pManger
        }
    }
})()

const processManger1 = singleton.getProcessManager()
const processManger2 = singleton.getProcessManager()

console.log(processManger1 === processManger2)